module bitbucket.org/spinnerweb/cards

go 1.15

require (
	bitbucket.org/spinnerweb/accounting_common v0.0.0-20201007124130-3b7977b096ed
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/googollee/go-socket.io v1.4.4
	github.com/gorilla/websocket v1.4.2
	github.com/maedu/mongo-go-pagination v0.0.4
	go.mongodb.org/mongo-driver v1.4.3
)
